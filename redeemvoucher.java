// Define a coupon with rate
//   rate > 1: cash coupon with face value = rate
//   rate < 1: discount coupon with rate of discount

import java.util.*;

class Coupon {

    /********* Data attributes **********/
    private String name;
    private double rate;

    /********** Constructors **********/
    public Coupon(){
    }

    public Coupon(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }

    /********* Accessors ************/
    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    // Computes the amount to pay.
    // If payment calculated is negative, it means that
    // the coupon's value is larger than the price.
    public double payment(double price) {
        if (rate > 1)
            return price-rate;
        else
            return (1-rate)*price;
    }
}
// This program reads in an item's cost and some coupons' information,
// and then determines which is the best coupon to use and the amount 
// to pay.

public class Redeem {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double price = sc.nextDouble();
        int numCoupons = sc.nextInt();

        Coupon[] coupons = new Coupon[numCoupons];

        for (int i=0; i<numCoupons; i++)
            coupons[i] = new Coupon(sc.next(), sc.nextDouble());

        int minIndex = 0;
        double minPrice = coupons[0].payment(price), couponPrice;

        for (int i=1; i<numCoupons; i++) {
            couponPrice = coupons[i].payment(price);
            if ((couponPrice > 0 && minPrice > 0 && couponPrice < minPrice) || (couponPrice < 0 && (minPrice > 0 || (minPrice < 0 && couponPrice > minPrice)))) {
                minPrice = couponPrice;
                minIndex = i;
            }
        }

        // Output the result

        System.out.println("Best choice: " + coupons[minIndex].getName());
        System.out.printf("You need to pay $%.2f\n",((minPrice < 0)? 0 : minPrice));
    }
}